import pygame
pygame.init()


def get_center(screen):
    x, y = screen.get_size()
    return (x//2, y//2)


def draw_scene(scr):
    scr.fill((255, 255, 255))
    #  pygame.draw.circle(scr, color=(200, 0, 0), center=(250, 250), radius=80)
    pygame.draw.circle(scr, color=(200, 0, 0), center=get_center(scr), radius=80)
    color = (0,0,255)
    pygame.draw.rect(scr, color, pygame.Rect(60, 60, 100, 100))
    pygame.draw.line(scr, color, (40, 300), (140, 300), 6)
    purple = (102, 0, 102)
    #  pygame.draw.polygon(scr, purple,
                    #  ((146, 0), (291, 106),(236, 277), (56, 277), (0, 106)))


scr = pygame.display.set_mode((600, 500))
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    draw_scene(scr)
    pygame.display.flip()

pygame.quit()
