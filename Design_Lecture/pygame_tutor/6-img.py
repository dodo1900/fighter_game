import pygame
pygame.init()


def get_center(screen):
    x, y = screen.get_size()
    return (x//2, y//2)

def draw_scene(screen):
    scr.fill((0, 0, 0))
    img = pygame.image.load("Python-Guides-New-Logo.png")
    img = pygame.transform.scale(img,(400,400))
    img = pygame.transform.flip(img, True, False)
    screen.blit(img,[0,0])


scr = pygame.display.set_mode((600, 500))
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    draw_scene(scr)
    pygame.display.update()

pygame.quit()
