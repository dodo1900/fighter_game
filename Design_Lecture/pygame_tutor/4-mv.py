import pygame
pygame.init()
window = pygame.display.set_mode((600, 600))
pygame.display.set_caption("Moving Object")
x = 200
y = 200
width = 20
height = 20
vel = 10



def draw_scene(screen, x, y, color=(0, 0, 255) ):
    screen.fill((255, 255, 255))
    pygame.draw.rect(screen, color, (x, y, width, height))




run = True
while run:
    pygame.time.delay(10)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
    keys = pygame.key.get_pressed()
    if keys[pygame.K_LEFT] and x>0:
        x -= vel
    if keys[pygame.K_RIGHT] and x<600-width:
        x += vel
    if keys[pygame.K_UP] and y>0:
        y -= vel
    if keys[pygame.K_DOWN] and y<600-height:
        y += vel

    color = (x%255, y%255, 255)

    draw_scene(window, x, y, color)
    pygame.display.update()

pygame.quit()

