import copy
import logging
import pdb

from conf import *
from scene_object import SceneObject

logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)
logging.basicConfig(format='%(asctime)s:%(name)s:%(levelname)s:%(message)s')

class Bullet(SceneObject):
    def __init__(self, scene, position, direction_vector, speed):
        super().__init__(scene, position, direction_vector, speed)
        self.rect.x = position[0]
        self.rect.y = position[1]
        self.load_images(2, 'images/bullet/flame_{}.png')
        self.attack_power = 9
        self.frames = 0

    def update(self) :
        if self.frames == 0:
            super().update()
            self.frames = 2

        self.frames -= 1
        self.kill_out_of_sight()



