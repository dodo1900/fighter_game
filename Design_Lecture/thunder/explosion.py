import pygame

from scene_object import SceneObject

class Explosion(SceneObject):
    def __init__(self, scene, center, img_label, kind_id=0):
        '''
        four kinds of explosion effects.
        kind_id: 0, 1, 2, 3
        '''
        super().__init__(scene)
        self.kind_id = kind_id
        self.img_label = img_label
        self.image = self.scene.explosion_anim[self.img_label][kind_id][0]
        self.rect = self.image.get_rect()
        self.rect.center = center
        self.frame = 0

    def update(self):
        self.frame += 1
        if self.frame == len(self.scene.explosion_anim[self.img_label][self.kind_id]):
            self.kill()
        else:
            center = self.rect.center
            self.image = self.scene.explosion_anim[self.img_label][self.kind_id][self.frame]
            self.rect = self.image.get_rect()
            self.rect.center = center
