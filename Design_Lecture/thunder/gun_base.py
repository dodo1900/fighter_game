from scene_object import SceneObject
from bullets import Bullet

import pygame

class GunBase(SceneObject):
    def __init__(self, scene):
        super().__init__(scene, position=[0,0], direction_vector=[0.0,0.0], speed=1.0, image_path='')
        self.demage = 0 # 0..100 in percentage form
        self.fire_positions = pygame.sprite.Group()
        pass

    def build_bullets(self):
        return []




