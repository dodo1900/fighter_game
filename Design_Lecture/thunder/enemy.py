
import pygame
import random
import time
from pygame.locals import *

from gun_base import GunBase

from spritesheet import SpriteSheet
from conf import *

from random import randint, uniform
vec = pygame.math.Vector2

class Enemy(GunBase):

    def __init__(self, scene, image = None, speed=1.0, scene_target=None):
        super().__init__(scene)
        self.scene_target = scene_target

        self.vel = vec(3, 0).rotate(uniform(0, 360))
        self.acc = vec(0, 0)
        self.pos = vec(0, 0)

    def wander(self):
        steer = vec(0, 0)
        future = self.pos + self.vel.normalize() * WANDER_DISTANCE
        target = future + vec(WANDER_RADIUS, 0).rotate(uniform(0, 360))
        steer = self.seek(self.scene_target.rect.center)
        self.draw_data = [future, target]
        return steer

    def seek(self, target):
        desired = (target - self.pos).normalize() * MAX_SPEED
        steer = (desired - self.vel)
        if steer.length_squared() > 0.2**2:
            steer.scale_to_length(0.2)
        return steer

    def update(self):
        wander = self.wander()
        seek = vec(0, 0)

        self.acc = seek + wander
        self.vel += self.acc
        self.pos += self.vel

        if self.pos.x > self.scene.rect.width - self.rect.width:
            self.pos.x = self.scene.rect.width - self.rect.width
        if self.pos.x < 0:
            self.pos.x = 0
        if self.pos.y > self.scene.rect.height - self.rect.height:
            self.pos.y = self.scene.rect.height - self.rect.height
        if self.pos.y < 0:
            self.pos.y = 0

        self.rect.topleft = self.pos
