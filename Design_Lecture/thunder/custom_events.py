import pygame

class CustomEvents:
    def __init__(self):
        # Create custom events for adding a new sprites
        self.add_cloud_event = pygame.USEREVENT + 2
        pygame.time.set_timer(self.add_cloud_event, 5000)

        self.b2_main_pod_on_event = pygame.USEREVENT + 3
        pygame.time.set_timer(self.b2_main_pod_on_event, 7000)

        self.b2_main_pod_off_event = pygame.USEREVENT + 4
        pygame.time.set_timer(self.b2_main_pod_off_event, 11000)

        self.b2_left_pod_on_event = pygame.USEREVENT + 5
        pygame.time.set_timer(self.b2_left_pod_on_event, 7500)

        self.b2_left_pod_off_event = pygame.USEREVENT + 6
        pygame.time.set_timer(self.b2_left_pod_off_event, 11500)

        self.b2_right_pod_on_event = pygame.USEREVENT + 7
        pygame.time.set_timer(self.b2_right_pod_on_event, 8000)

        self.b2_right_pod_off_event = pygame.USEREVENT + 8
        pygame.time.set_timer(self.b2_right_pod_off_event, 15000)
