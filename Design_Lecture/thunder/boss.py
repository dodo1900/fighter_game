
import pygame
import random
import time
from pygame.locals import *
from pygame import Vector2 as vec

from random import randint
from custom_events import CustomEvents
from scene_object import SceneObject
from gun_base import GunBase
from bullets import Bullet
from enemy import Enemy
from explosion import Explosion

from spritesheet import SpriteSheet
from conf import *

import json
import pdb

logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)
logging.basicConfig(format='%(asctime)s:%(name)s:%(levelname)s:%(message)s')


sprite_rects = [
    {"fly":"0","x":21-6,"y":20,"width":191,"height":90-2},
    {"fly":"1","x":223-6,"y":19,"width":191,"height":90-2},
    {"fly":"2","x":425-6,"y":19,"width":191,"height":90-2},
    {"fly":"3","x":627-6,"y":19,"width":191,"height":90-2},
    {"fly":"4","x":20-6,"y":132,"width":191,"height":90-2},
    {"fly":"5","x":222-6,"y":131,"width":191,"height":90-2},
    {"fly":"6","x":425-6,"y":129,"width":191,"height":90-2},
    {"fly":"7","x":628-6,"y":128,"width":191,"height":90-2},
    {"left_wing":"0","x":22,"y":252,"width":53,"height":55},
    {"left_wing":"1","x":86,"y":252,"width":53,"height":55},
    {"left_wing":"2","x":150,"y":252,"width":53,"height":55},
    {"left_wing":"3","x":216,"y":252,"width":53,"height":55},
    {"left_wing":"4","x":284,"y":252,"width":53,"height":55},
    {"left_wing":"5","x":351,"y":252,"width":53,"height":55},
    {"left_wing":"6","x":420,"y":252,"width":53,"height":55},
    {"left_wing":"7","x":486,"y":252,"width":53,"height":55},
    {"right_wing":"0","x":24,"y":339,"width":52,"height":54},
    {"right_wing":"1","x":89,"y":339,"width":52,"height":54},
    {"right_wing":"2","x":151,"y":339,"width":52,"height":54},
    {"right_wing":"3","x":218,"y":339,"width":52,"height":54},
    {"right_wing":"4","x":285,"y":339,"width":52,"height":54},
    {"right_wing":"5","x":352,"y":339,"width":52,"height":54},
    {"right_wing":"6","x":415,"y":339,"width":52,"height":54},
    {"right_wing":"7","x":481,"y":339,"width":52,"height":54}
]


class BossB2(Enemy):

    def __init__(self, scene, image = None, speed = 1.0, target=None):
        super().__init__(scene, target)

        self.main_pod_ready = False
        self.left_wing_pod_ready = False # [close, open]
        self.right_wing_pod_ready = False    # [close, open]


        self.left_wing_demaged = False
        self.right_wing_demaged = False
        self.body_demaged = False

        # hit test
        self.left_wing_cnt = 3
        self.right_wing_cnt = 3
        self.body_cnt = 3


        # load sprites from spritesheet file
        ss = SpriteSheet('images/boss_b2.png')
        #  pdb.set_trace()
        rects = [(e['x'], e['y'], e['width'], e['height'])
                 for e in filter(lambda x: 'fly' in x.keys(), sprite_rects)
                 ]
        self.b2_body = ss.images_at(rects)

        rects = [(e['x'], e['y'], e['width'], e['height'])
                 for e in filter(lambda x: 'left_wing' in x.keys(), sprite_rects)
                 ]

        self.b2_left_wing = ss.images_at(rects)

        rects = [(e['x'], e['y'], e['width'], e['height'])
                 for e in filter(lambda x: 'right_wing' in x.keys(), sprite_rects)
                 ]

        self.b2_right_wing = ss.images_at(rects)

        self.current_body_number = 0
        self.current_left_wing_number = 0
        self.current_right_wing_number = 0

        self.image = self.b2_body[self.current_body_number]
        self.left_wing = self.b2_left_wing[self.current_left_wing_number]
        self.right_wing = self.b2_right_wing[self.current_right_wing_number]
        self.speed = speed

        self.rect = self.image.get_rect()
        self.position = [50, 230]

        self.frames = FRAMES

    def set_main_pod_ready(self, state = False):
        self.main_pod_ready = state

    def set_left_wing_pod_ready (self, state = False):
        self.left_wing_pod_ready = state

    def set_right_wing_pod_ready (self, state = False):
        self.right_wing_pod_ready = state

    def set_target(self, scene_target):
        self.scene_target = scene_target


    def update(self):

        if self.frames == 0:
            if self.main_pod_ready: # openning
                self.current_body_number = min((self.current_body_number + 1),
                    len(self.b2_body)-2 )  # the last frame: destoried
            elif self.current_body_number > 0:
                self.current_body_number = max((self.current_body_number - 1),
                                                    0)

            if self.body_demaged:
                self.image = self.b2_body[len(self.b2_body)-1] # the last frame demaged body
            else:
                self.image = self.b2_body[self.current_body_number]


            if self.left_wing_pod_ready: # openning
                self.current_left_wing_number = min((self.current_left_wing_number + 1),
                len(self.b2_left_wing)-1)
            elif self.current_left_wing_number > 0:
                self.current_left_wing_number = max((self.current_left_wing_number - 1),
                                                    0)
            if self.right_wing_pod_ready: # openning
                self.current_right_wing_number = min((self.current_right_wing_number + 1),
                len(self.b2_right_wing)-1)
            elif self.current_right_wing_number > 0:
                self.current_right_wing_number = max((self.current_right_wing_number - 1),
                                                     0)

            self.left_wing = self.b2_left_wing[self.current_left_wing_number]
            self.right_wing = self.b2_right_wing[self.current_right_wing_number]
            self.frames = FRAMES

        self.frames -= 1
        self.sound_cnt -= 1


        super().update()


    def draw(self):
        l_offset=vec(-5, 40)
        r_offset=vec(156, 40)
        p = vec(self.position)

        self.scene.screen.blit(self.image, self.position)
        if not self.left_wing_demaged:
            self.scene.screen.blit(self.left_wing, p+l_offset)
        if not self.right_wing_demaged:
            self.scene.screen.blit(self.right_wing, p+r_offset)


    def hit(self, objects):
        super().hit(objects)
        # play sounds
        for o in objects:
            ex = Explosion(self.scene, o.rect.center, 'small')
            self.scene.add(ex)

        self.left_wing_cnt -= 1
        if self.left_wing_demaged:
            self.right_wing_cnt -= 1
        if self.right_wing_demaged:
            self.body_cnt -= 1

        logger.debug('l:%d r:%d B:%d' % (self.left_wing_cnt,
                                         self.right_wing_cnt,
                                         self.body_cnt))
        # demage status
        self.left_wing_demaged = self.left_wing_cnt < 0
        self.right_wing_demaged = self.right_wing_cnt < 0
        self.body_demaged = self.body_cnt < 0

        # body feedback
        # self.image.fill((255, 0, 0), special_flags=BLEND_ADD)
        if self.left_wing_demaged or self.right_wing_demaged:
            self.big_bang(o.rect.center)

        if self.body_demaged:
            self.big_bang(self.rect.center)


    def big_bang(self, position):
        self.scene.sound_explo.play()
        # two medium explos
        p = pygame.Vector2(position)
        offset = pygame.Vector2(randint(-30,30), randint(-20,20))
        em = Explosion(self.scene, p+offset, 'medium', kind_id=randint(0,3))
        self.scene.add(em, layer=2)
        offset = pygame.Vector2(randint(-30,30), randint(-20,20))
        em = Explosion(self.scene, p+offset, 'medium', kind_id=randint(0,3))
        self.scene.add(em, layer=3)

        # three big explos
        offset = pygame.Vector2(randint(-60,60), randint(-20,20))
        em = Explosion(self.scene, p+offset, 'big', kind_id=randint(0,3))
        self.scene.add(em, layer=4)
        offset = pygame.Vector2(randint(-60,60), randint(-20,20))
        em = Explosion(self.scene, p+offset, 'big', kind_id=randint(0,3))
        self.scene.add(em, layer=5)
        self.scene.sound_explo_remote.play()
        offset = pygame.Vector2(randint(-40,40), randint(-20,20))
        em = Explosion(self.scene, p+offset, 'huge', kind_id=randint(0,3))
        self.scene.add(em, layer=3)

        # two medium explos
        offset = pygame.Vector2(randint(-30,30), randint(-20,20))
        em = Explosion(self.scene, p+offset, 'medium', kind_id=randint(0,3))
        self.scene.add(em, layer=7)
        offset = pygame.Vector2(randint(-30,30), randint(-20,20))
        em = Explosion(self.scene, p+offset, 'medium', kind_id=randint(0,3))
        self.scene.add(em, layer=7)

        pass
