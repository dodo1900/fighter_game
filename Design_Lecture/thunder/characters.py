
import pygame
import random
import time
from pygame.locals import *
from pygame import Surface

from custom_events import CustomEvents
from scene_object import SceneObject
from gun_base import GunBase
from bullets import Bullet

from conf import *

logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)
logging.basicConfig(format='%(asctime)s:%(name)s:%(levelname)s:%(message)s')

class FadeEffect(SceneObject):

    def __init__(self, scene, width=WIDTH, height=HEIGHT, alpha_start=0,
                 alpha_end=255, alpha_inc=20, color=(255,255,255)):
        super().__init__(scene)
        self.alpha_surface = Surface((width, height)) # The custom-surface of the size of the screen.
        self.alpha_surface.fill(color) # Fill it with whole white before the main-loop.
        self.alpha_surface.set_alpha(0) #
        self.active = False
        self.alpha = alpha_start
        self.alpha_start = alpha_start
        self.alpha_inc = alpha_inc
        self.alpha_end = alpha_end

        self.frame_counter = 30
        self.done = False

    def update(self):
        logger.debug(self.alpha)
        if self.active:
            self.alpha += self.alpha_inc # Increment alpha by a really small value (To make it slower, try 0.01)
            if abs(self.alpha - self.alpha_end) < abs(self.alpha_inc):
                self.alpha = self.alpha_end
            self.alpha_surface.set_alpha(self.alpha) # Set the incremented alpha-value to the custom surface.
            self.frame_counter -= 1
            self.done = (self.frame_counter < 0)

    def reset(self):
        self.active = False;
        self.alpha = self.alpha_start
        self.done = False

    def draw(self):
         self.scene.screen.blit(self.alpha_surface,(0,0)) # Blit it to the screen-surface (Make them separate)



class GameBackgroud(SceneObject):

    def __init__(self, scene, image = None, speed = 1.0):
        super().__init__(scene)
        self.y = None

        self.image = image
        self.speed = speed

        self.rect = pygame.Rect([0,0,1,1]) # dummy rect
        if self.image is not None:
            self.rect = self.image.get_rect()
        self.rect_up = self.rect.copy()

        self.reset_position()

    def update(self):
        self.y = int(self.y + self.speed) % self.rect.height

    def draw(self):
        self.scene.screen.blit(self.image, (0, self.y))
        self.scene.screen.blit(self.image, (0, self.y - self.rect.height))

    def reset_position(self):
        self.y = 0


class Clouds(SceneObject):
    def __init__(self, scene, position, direction_vector, speed):
        super().__init__(scene, position, direction_vector, speed,
                         image_path='images/clouds.png')
        self.initial_position = position


    def reset_position(self):
        x, y = self.initial_position
        self.position = [x - random.randint(1, 10) * 30, y]


class Fighter(GunBase):
    def __init__(self, scene):
        super().__init__(scene)


        # Fighter states: 'left', 'shoot_ahead', 'right'
        self.current_state = 'shoot_ahead'

        self.speed = 8.0
        self.load_images(5, 'images/jets/thunder_{}.png')

        self.current_jet_image_number = 2
        self.image = self.image_bank[self.current_jet_image_number]

        self.rect = self.image.get_rect()
        self.position = self.scene.rect.width // 2 - self.rect.width // 2, 600
        self.shoot_cooldown = 0


    def move(self, direction_vector, speed):
        x, y = self.position
        self.position  = [int(x + direction_vector[0] * speed),
                int(y + direction_vector[1] * speed) ]
        if not (self.scene.rect.collidepoint(self.rect.topleft) \
                and self.scene.rect.collidepoint(self.rect.bottomright)):
            self.position = [x, y] # using old pos


    def set_ahead(self):
        if self.current_state == 'shoot_ahead':
            if self.current_jet_image_number > 2:
                self.current_jet_image_number -= 1
            elif self.current_jet_image_number < 2:
                self.current_jet_image_number += 1

    def move_up(self):
        self.move([0, -1], self.speed)

    def move_down(self):
        self.move([0, 1], self.speed)

    def move_left(self):
        self.current_jet_image_number = min(4, self.current_jet_image_number + 1)
        self.move([-1, 0], self.speed)

    def move_right(self):
        self.current_jet_image_number = max(0, self.current_jet_image_number - 1)
        self.move([1, 0], self.speed)

    def build_bullets(self):
        logger.debug(self.rect)
        b = Bullet(self.scene, [0, 0], [0, -1], 60)
        x, y = self.position
        x = x + self.rect.width // 2 \
            - b.rect.width // 2

        b.position = (x, y)

        return [b]

    def shoot(self):
        if self.shoot_cooldown == 0:
            # silly to do this
            #  self.scene.sound_gun_shots.play()
            b = self.build_bullets()
            self.scene.add(b)
            self.scene.jet_bullets.add(b)
            self.shoot_cooldown = 20


    def update(self):
        if self.shoot_cooldown > 0:
            self.shoot_cooldown -= 1

        # update current jet view
        self.image = self.image_bank[self.current_jet_image_number]

    def draw(self):
        self.scene.screen.blit(self.image, self.position)


