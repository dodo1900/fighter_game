
import pygame
import random
import time
from pygame.locals import *

from custom_events import CustomEvents
from scene_object import SceneObject
from scene import Scene
from conf import *
from scene_runner import SceneRunner

class Game:
    __isinstance = False
    def __new__(cls, *args, **kwargs):
        if not cls.__isinstance:
            cls.__isinstance = object.__new__(cls)
        return cls.__isinstance

    def __init__(self):
        pygame.init()

        size = (WIDTH, HEIGHT)
        screen = pygame.display.set_mode(size)
        pygame.display.set_caption('Thunder Fighter')

        game_event = CustomEvents()
        green_land = Scene(screen, game_event)

        runner = SceneRunner(green_land)
        runner.play_music("music/chengdu.mp3")
        runner.run_scene()

        # after background music faded out
        pygame.time.delay(1000)
        pygame.quit()
        exit()


Game()
## end of file


